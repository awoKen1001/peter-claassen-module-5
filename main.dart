import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:squeakyclean/adduser.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: FirebaseOptions(
      apiKey: "AIzaSyAZLoMPbd97cST7jT20HmY0NCQuRcBv_c0", // Your apiKey
      appId: "1:86479957093:android:f39ec6e6d34ca35e89aa8c", // Your appId
      messagingSenderId: "XXX", // Your messagingSenderId
      projectId: "squeakyclean-11088", // Your projectId
    ),
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Squeaky Clean'),
      //home: AddUser(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var tecName = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('HomePage'),
        actions: [
          IconButton(
            padding: const EdgeInsets.fromLTRB(25.0, 15.0, 25.0, 15.0),
            iconSize: 32,
            onPressed: () {
              final name = tecName.text;

              //createUser(name: name);
            },
            icon: Icon(
              Icons.add,
            ),
          ),
        ],
      ),
      body: StreamBuilder<List<User>>(
        stream: readUsers(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text('Something went wrong');
          } else if (snapshot.hasData) {
            final users = snapshot.data!;

            return ListView(
              children: users.map(buildUser).toList(),
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }

  Widget buildUser(User user) => ListTile(
        leading: CircleAvatar(
          radius: 50,
          child: Text('${user.surname}'),
        ),
        onTap: () {
          // Navigator.pushNamed(context, AddUser());
        },
        title: Text(user.name),
        subtitle: Text(user.hometown),
      );

  Stream<List<User>> readUsers() => FirebaseFirestore.instance
      .collection('users')
      .snapshots()
      .map((snapshot) =>
          snapshot.docs.map((doc) => User.FromJson(doc.data())).toList());
}

class User {
  String id;
  final String name;
  final String surname;
  final String hometown;

  User(
      {this.id = '',
      required this.name,
      required this.surname,
      required this.hometown});

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'surname': surname,
        'hometown': hometown,
      };

  static User FromJson(Map<String, dynamic> json) => User(
      id: json['id'],
      name: json['name'],
      surname: json['surname'],
      hometown: json['hometown']);
}
