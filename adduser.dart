import 'package:flutter/material.dart';
import 'package:squeakyclean/main.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class AddUser extends StatefulWidget {
  @override
  State<AddUser> createState() => _AddUserState();
}

class _AddUserState extends State<AddUser> {
  final tecName = TextEditingController();
  final tecSurname = TextEditingController();
  final tecHometown = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add New User'),
      ),
      body: Column(
        children: [
          const SizedBox(
            height: 20,
          ),
          Container(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              controller: tecName,
              decoration: InputDecoration(
                hintText: 'Name',
              ),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              controller: tecSurname,
              decoration: InputDecoration(
                hintText: 'Surname',
              ),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              controller: tecHometown,
              decoration: InputDecoration(
                hintText: 'Hometown',
              ),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Colors.cyan,
              ),
              child: Text('Add User'),
              onPressed: () {
                final user = User(
                    name: tecName.text,
                    surname: tecSurname.text,
                    hometown: tecHometown.text);

                createUser(user);

                Navigator.pop(context);
              },
            ),
          ),
        ],
      ),
    );
  }

  Future createUser(User user) async {
    final docUser = FirebaseFirestore.instance.collection('users').doc();
    user.id = docUser.id;

    final json = user.toJson();
    await docUser.set(json);
  }
}
